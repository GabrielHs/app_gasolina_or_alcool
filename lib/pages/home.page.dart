import 'package:aog/widgets/logo.widget.dart';
import 'package:aog/widgets/submit-form.widget.dart';
import 'package:aog/widgets/sucess.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color _color = Colors.deepPurple;

  var _gasCtrl = new MoneyMaskedTextController();

  var _alcCtrl = new MoneyMaskedTextController();

  var _busy = false;

  var _completed = false;

  var result = "Compensa utilizar álcool";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: AnimatedContainer(
          duration: Duration(
            milliseconds: 1200,
          ),
          child: ListView(
            children: <Widget>[
              Logo(),
              // Success(result: "Compensa utilizar a", reset: () {}),
              _completed
                  ? Success(
                      result: result,
                      reset: reset,
                    )
                  : SubmitForm(
                      gastCtrl: _gasCtrl,
                      aclCtrl: _alcCtrl,
                      busy: _busy,
                      submitFunc: calculed,
                    )
            ],
          ),
          color: _color,
        ));
  }

  Future calculed() {
    double acl =
        double.parse(_alcCtrl.text.replaceAll(new RegExp(r'[,.]'), '')) / 100;
    double gas =
        double.parse(_gasCtrl.text.replaceAll(new RegExp(r'[,.]'), '')) / 100;

    double res = acl / gas;

    setState(() {
      _color = Colors.deepPurpleAccent;
      _completed = false;
      _busy = true;
    });

    return new Future.delayed(
      const Duration(seconds: 2),
      () => setState(() {
        if (res >= 0.7) {
          result = "Compensa utlizar Gasolina!";
        } else {
          result = "Compensa utlizar Álcool!";
        }
        _completed = true;
        _busy = false;
      }),
    );
  }

  reset() {
    setState(() {
      _color = Colors.deepPurple;
      _alcCtrl = new MoneyMaskedTextController();
      _gasCtrl = new MoneyMaskedTextController();
      _completed = false;
      _busy = false;
    });
  }
}
