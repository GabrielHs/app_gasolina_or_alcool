import 'package:aog/widgets/Input.widget.dart';
import 'package:aog/widgets/loading-button.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

// ignore: must_be_immutable
class SubmitForm extends StatelessWidget {
  var gastCtrl = new MoneyMaskedTextController();
  var aclCtrl = new MoneyMaskedTextController();
  var busy = false;
  Function submitFunc;

  SubmitForm({
    @required this.gastCtrl,
    @required this.aclCtrl,
    @required this.busy,
    @required this.submitFunc,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          child: InputGaso(
            label: "Gasolina",
            ctrl: gastCtrl,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          child: InputGaso(
            label: "Álcool",
            ctrl: aclCtrl,
          ),
        ),
        SizedBox(
          height: 25,
        ),
        LoadingButtonG(
          busy: busy,
          func: submitFunc,
          invert: false,
          text: "Calcular",
        )
      ],
    );
  }
}
